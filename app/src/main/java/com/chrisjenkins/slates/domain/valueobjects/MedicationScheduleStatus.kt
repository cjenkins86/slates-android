package com.chrisjenkins.slates

enum class ScheduleStatusType (status:Int) {
    NotStarted(0),
    WaitingToComplete(1),
    Done(2)
}

data class MedicationScheduleStatus (val id:String, val status: ScheduleStatusType)