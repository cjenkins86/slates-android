package com.chrisjenkins.slates.domain.valueobjects

enum class MedicationStatus (val status:Int) {
    Unknown(-1),
    WaitingForNextDose(0),
    DoseIsOverdue(1)
}