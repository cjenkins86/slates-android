package com.chrisjenkins.slates.domain.interactors

import com.chrisjenkins.slates.MedicationId
import com.chrisjenkins.slates.MedicationRepository
import com.chrisjenkins.slates.domain.entities.Medication
import com.chrisjenkins.slates.domain.valueobjects.CreateMedication
import io.reactivex.Observable

class MedicationDomainInteractor (
    private val medicationRepository: MedicationRepository
) {

    fun createMedication (create: CreateMedication) : Observable<MedicationId> {
        val medId = medicationRepository.generateId()
        val med = Medication(
            id = medId, name = create.name, maxQuantity = create.quantity,
            milligrams = create.milligrams, time = create.duration
        )

        return medicationRepository.save(med)
    }
}