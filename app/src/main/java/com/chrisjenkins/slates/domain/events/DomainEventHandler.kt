package com.chrisjenkins.slates.domain.events

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*

data class DomainEventId (val unique:String)

open class DomainEvent (val id: DomainEventId, val name:String)

private val domainEventSubject : PublishSubject<DomainEvent> = PublishSubject.create()
val onDomainEvent : Observable<DomainEvent> =
    domainEventSubject

fun triggerDomainEvent (event: DomainEvent) {
    domainEventSubject.onNext(event)
}

fun generateDomainEventId () : DomainEventId {
    return DomainEventId(UUID.randomUUID().toString())
}