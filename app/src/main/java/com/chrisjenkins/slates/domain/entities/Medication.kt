package com.chrisjenkins.slates.domain.entities

import com.chrisjenkins.slates.*
import com.chrisjenkins.slates.domain.events.generateDomainEventId
import com.chrisjenkins.slates.domain.events.triggerDomainEvent
import com.chrisjenkins.slates.domain.valueobjects.Dosage
import com.chrisjenkins.slates.domain.valueobjects.MedicationStatus
import com.chrisjenkins.slates.integration.locators.MedicationDateLocator
import org.threeten.bp.Duration

class Medication (
    val id: MedicationId,
    val name:String,
    val maxQuantity:Int,
    val milligrams:Int,
    val time:Duration,
    private var _dosages: MutableList<Dosage> = mutableListOf()){

    val dosages : List<Dosage>
        get() = _dosages

    val status : MedicationStatus = MedicationStatus.Unknown

    fun takeDosage () {
        if (_dosages.count() < maxQuantity) {
            val dosage = Dosage(
                milligrams,
                MedicationDateLocator.date.currentTime()
            )

            _dosages.add(dosage)

            triggerDomainEvent(
                TookMedicationDosage(
                    generateDomainEventId(),
                    id
                )
            )
        }
    }
}