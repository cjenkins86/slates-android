package com.chrisjenkins.slates.domain.valueobjects

import org.threeten.bp.Instant
import java.util.*

data class Dosage (val milligrams:Int, val time:Instant)