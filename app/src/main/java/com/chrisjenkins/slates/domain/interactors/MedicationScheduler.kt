package com.chrisjenkins.slates

import io.reactivex.Observable

interface MedicationScheduler {

    val eventReceiver : Observable<MedicationScheduleStatus>

    fun start (id:MedicationId, hours:Int) : Observable<MedicationScheduleStatus>
    fun stop (id:MedicationId) : Observable<MedicationScheduleStatus>
    fun get (id:MedicationId) : Observable<MedicationScheduleStatus>
}