package com.chrisjenkins.slates

import org.threeten.bp.Instant

interface MedicationDate {
    fun currentTime(): Instant
}