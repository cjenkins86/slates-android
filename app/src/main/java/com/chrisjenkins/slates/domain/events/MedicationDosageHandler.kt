package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.events.DomainEvent
import com.chrisjenkins.slates.domain.events.DomainEventId
import com.chrisjenkins.slates.domain.events.onDomainEvent
import com.chrisjenkins.slates.integration.events.MedicationStatusEvent

class TookMedicationDosage (id: DomainEventId, val medId:MedicationId)
    : DomainEvent(id, NAME) {
    companion object {
        val NAME = "TookMedicationDosage"
    }
}

class MedicationDosageHandler (private val medicationScheduler: MedicationScheduler,
                               private val medicationRepository: MedicationRepository) {
    init {
        onDomainEvent
            .filter { it.name ==  TookMedicationDosage.NAME }
            .map { it as TookMedicationDosage }
            .flatMap { event -> medicationRepository.find(event.medId).toObservable() }
            .flatMap { med -> medicationScheduler.start(med.id, med.time.toHours().toInt()) }
            .subscribe { status ->
                // ..broadcast event to update GUI of new scheduler for medical dosage
                triggerIntegrationEvent(MedicationStatusEvent(generateIntegrationEventId(), status))
            }
    }
}