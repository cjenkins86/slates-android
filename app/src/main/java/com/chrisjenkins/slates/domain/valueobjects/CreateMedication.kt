package com.chrisjenkins.slates.domain.valueobjects

import org.threeten.bp.Duration


data class CreateMedication (val name:String, val quantity:Int, val milligrams:Int, val duration: Duration)