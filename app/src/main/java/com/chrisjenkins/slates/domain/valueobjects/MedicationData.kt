package com.chrisjenkins.slates

data class MedicationData (val id:String, val name:String, val maxQuantity:Int,
                           val milligrams:Int, val hours:Int)