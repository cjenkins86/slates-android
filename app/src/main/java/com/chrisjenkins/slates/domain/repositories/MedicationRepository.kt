package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.entities.Medication
import io.reactivex.Observable
import io.reactivex.Single

interface MedicationRepository {
    fun generateId () : MedicationId
    fun all () : Observable<List<Medication>>
    fun find (id:MedicationId) : Single<Medication>
    fun save (medication: Medication) : Observable<MedicationId>
    fun remove (id:MedicationId) : Observable<Boolean>
}