package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.entities.Medication
import io.reactivex.Observable
import io.reactivex.Single
import java.util.*

class MedicationMemoryRepository (medications:List<Medication> = emptyList())
    : MedicationRepository {

    private var _medications:MutableList<Medication> = medications.toMutableList()

    override fun save(medication: Medication): Observable<MedicationId> {
        _medications.add(medication)
        return Observable.just(medication.id)
    }

    override fun find(id: MedicationId): Single<Medication> {
        return Single.just(_medications.first { it.id.unique == id.unique })
    }

    override fun generateId(): MedicationId {
        return MedicationId(UUID.randomUUID().toString())
    }

    override fun all(): Observable<List<Medication>> {
        return Observable.just(_medications)
    }

    override fun remove(id: MedicationId): Observable<Boolean> {
        return Observable.just(_medications.removeAll { it.id.unique == id.unique })
    }
}