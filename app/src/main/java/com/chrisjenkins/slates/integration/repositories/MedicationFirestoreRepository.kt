package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.entities.Medication
import io.reactivex.Observable
import java.util.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import io.reactivex.Single
import io.reactivex.subjects.AsyncSubject
import org.threeten.bp.Duration


class MedicationFirestoreRepository : MedicationRepository {

    private var _medications : MutableList <Medication> = mutableListOf()
    private var _dataDirty: Boolean = true

    override fun generateId () : MedicationId {
        return MedicationId(UUID.randomUUID().toString())
    }

    override fun find (id:MedicationId) : Single<Medication> {
        if (!_dataDirty) {
            val med = _medications.first { it.id.unique == id.unique }
            return Single.just(med)
        }

        return all().flatMap { v -> Observable.fromIterable(v) }
            .filter { it.id.unique == id.unique }
            .firstOrError()
    }

    override fun all () : Observable<List<Medication>> {
        if (!_dataDirty) {
            return Observable.just(_medications)
        }

        return Observable.defer {
            val async = AsyncSubject.create<List<Medication>>()
            val db = FirebaseFirestore.getInstance()
            db.collection("medications")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        val medications: MutableList<Medication> = task.result!!
                            .map { convertFromDocument(it) }
                            .toMutableList()

                        _medications = medications
                        _dataDirty = false

                        async.onNext(medications)
                        async.onComplete()
                    } else {
                        async.onComplete()
                    }
                }

            async
        }
    }

    override fun save (medication: Medication) : Observable<MedicationId> {
        return Observable.defer {
            val async = AsyncSubject.create<MedicationId>()
            val db = FirebaseFirestore.getInstance()
            val medData = convertToData(medication)
            db.collection("medications")
                .document(medication.id.unique)
                .set(medData)
                .addOnSuccessListener {
                    async.onNext(medication.id)
                    async.onComplete()
                }
                .addOnFailureListener { ex ->
                    async.onComplete()
                }

            async
        }
    }

    override fun remove (id:MedicationId) : Observable<Boolean> {
        return Observable.defer {
            val async = AsyncSubject.create<Boolean>()
            val db = FirebaseFirestore.getInstance()
            db.collection("medications")
                .document(id.unique)
                .delete()
                .addOnSuccessListener {
                    _medications.removeAll { it.id.unique == id.unique }
                    async.onNext(true)
                    async.onComplete()
                }
                .addOnFailureListener {
                    async.onComplete()
                }

            async
        }
    }

    private fun convertToData (medication: Medication) : MedicationData {
        return MedicationData(
            id = medication.id.unique,
            name = medication.name,
            maxQuantity = medication.maxQuantity,
            milligrams = medication.milligrams,
            hours = medication.time.toHours().toInt()
        )
    }

    private fun convertFromDocument(doc:QueryDocumentSnapshot) : Medication {
        return Medication(
            id = MedicationId(doc.id),
            name = doc.getString("name")!!,
            maxQuantity = doc.getLong("maxQuantity")!!.toInt(),
            milligrams = doc.getLong("milligrams")!!.toInt(),
            time = Duration.ofHours(doc.getLong("hours")!!)
        )
    }
}