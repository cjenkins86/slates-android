package com.chrisjenkins.slates.integration.events

import com.chrisjenkins.slates.IntegrationEvent
import com.chrisjenkins.slates.IntegrationEventId
import com.chrisjenkins.slates.MedicationScheduleStatus

class MedicationStatusEvent (id:IntegrationEventId, val status:MedicationScheduleStatus)
    : IntegrationEvent (id, NAME) {
    companion object {
        const val NAME = "MedicationStatusEvent"
    }
}