package com.chrisjenkins.slates.integration

import com.chrisjenkins.slates.MedicationId
import com.chrisjenkins.slates.MedicationScheduleStatus
import com.chrisjenkins.slates.MedicationScheduler
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class OfflineMedicationScheduler : MedicationScheduler {
    private val eventReceiverSubject : PublishSubject<MedicationScheduleStatus> = PublishSubject.create()
    override val eventReceiver : Observable<MedicationScheduleStatus> = eventReceiverSubject

    override fun start (id: MedicationId, hours:Int) : Observable<MedicationScheduleStatus> {
        return Observable.empty()
    }

    override fun stop (id: MedicationId) : Observable<MedicationScheduleStatus> {
        return Observable.empty()
    }

    override fun get (id: MedicationId) : Observable<MedicationScheduleStatus> {
        return Observable.empty()
    }
}