package com.chrisjenkins.slates.integration.locators

import com.chrisjenkins.slates.MedicationDate
import org.threeten.bp.Instant

class DefaultMedicationDate : MedicationDate {
    override fun currentTime(): Instant {
        return Instant.now()
    }
}