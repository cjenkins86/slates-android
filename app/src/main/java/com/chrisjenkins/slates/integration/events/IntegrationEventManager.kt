package com.chrisjenkins.slates

import io.reactivex.subjects.PublishSubject
import java.util.*

class IntegrationEventId (val unique:String)

open class IntegrationEvent (val id:IntegrationEventId, val name:String)

private val integrationEventSubject : PublishSubject<IntegrationEvent> = PublishSubject.create()

val onIntegrationEvent = integrationEventSubject

fun triggerIntegrationEvent (event:IntegrationEvent) {
    integrationEventSubject.onNext(event)
}

fun generateIntegrationEventId () : IntegrationEventId {
    return IntegrationEventId(UUID.randomUUID().toString())
}