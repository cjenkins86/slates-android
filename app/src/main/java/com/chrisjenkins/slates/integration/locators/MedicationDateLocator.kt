package com.chrisjenkins.slates.integration.locators

import com.chrisjenkins.slates.MedicationDate

class MedicationDateLocator {
    companion object {
        var date: MedicationDate = DefaultMedicationDate()
    }
}