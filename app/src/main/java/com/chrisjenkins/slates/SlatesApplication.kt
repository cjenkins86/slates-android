package com.chrisjenkins.slates

import android.app.Application
import com.chrisjenkins.slates.di.*
import com.jakewharton.threetenabp.AndroidThreeTen

class SlatesApplication : Application() {

    val appComponent : ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    val medicationListComponent : MedicationListComponent by lazy {
        appComponent.medicationListComponent(MedicationListModule())
    }

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
    }
}