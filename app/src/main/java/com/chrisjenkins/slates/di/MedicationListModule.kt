package com.chrisjenkins.slates.di

import com.chrisjenkins.slates.MedicationListPresenter
import com.chrisjenkins.slates.MedicationRepository
import com.chrisjenkins.slates.domain.interactors.MedicationDomainInteractor
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers

@Module
class MedicationListModule {
    @Provides
    @ViewScope
    fun providesMedicationListPresenter (medicationRepository: MedicationRepository,
                                         medicationInteractor: MedicationDomainInteractor)
        : MedicationListPresenter {
        return MedicationListPresenter(medicationRepository, medicationInteractor, AndroidSchedulers.mainThread())
    }
}