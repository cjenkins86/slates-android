package com.chrisjenkins.slates.di

import android.app.Application
import com.chrisjenkins.slates.*
import com.chrisjenkins.slates.domain.interactors.MedicationDomainInteractor
import com.chrisjenkins.slates.integration.OfflineMedicationScheduler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule (private val app: SlatesApplication) {
    @Provides
    @Singleton
    fun provideApplication(): Application {
        return app
    }

    @Provides
    @Singleton
    fun provideMedicationRepository() : MedicationRepository {
        return MedicationFirestoreRepository()
    }

    @Provides
    @Singleton
    fun provideMedicationScheduler() : MedicationScheduler {
        return OfflineMedicationScheduler()
    }

    @Provides
    @Singleton
    fun provideMedicationDosageHandler (scheduler: MedicationScheduler, repository: MedicationRepository)
            : MedicationDosageHandler {
        return MedicationDosageHandler(scheduler, repository)
    }

    @Provides
    fun provideMedicationDomainInteractor (repository: MedicationRepository) : MedicationDomainInteractor {
        return MedicationDomainInteractor(repository)
    }
}