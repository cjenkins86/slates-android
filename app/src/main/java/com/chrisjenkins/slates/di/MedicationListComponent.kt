package com.chrisjenkins.slates.di

import com.chrisjenkins.slates.MedicationListController
import dagger.Subcomponent

@ViewScope
@Subcomponent(modules = [MedicationListModule::class])
interface MedicationListComponent {
    fun inject (controller: MedicationListController)
}