package com.chrisjenkins.slates.di

import dagger.Component
import javax.inject.Singleton

@Component(modules = [ApplicationModule::class])
@Singleton
interface ApplicationComponent {
    fun medicationListComponent (module: MedicationListModule) : MedicationListComponent
}