package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.entities.Medication
import com.chrisjenkins.slates.domain.interactors.MedicationDomainInteractor
import com.chrisjenkins.slates.domain.valueobjects.CreateMedication
import com.chrisjenkins.slates.integration.events.MedicationStatusEvent
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.threeten.bp.Duration

class MedicationListPresenter (private val medicationRepository:MedicationRepository,
                               private val interactor:MedicationDomainInteractor,
                               private val uiScheduler:Scheduler) {

    private lateinit var _view:MedicationListView

    private var _disposables: CompositeDisposable = CompositeDisposable()

    fun attachView (view:MedicationListView) {

        _view = view

        retrieveAll()
            .subscribeOn(uiScheduler)
            .subscribe{ items -> view.renderItems(items) }

        _disposables.add(onIntegrationEvent
            .filter { event -> event.name == MedicationStatusEvent.NAME }
            .map { (it as MedicationStatusEvent) }
            .flatMap { event -> medicationRepository.find(MedicationId(event.status.id))
                .map { med -> Pair(event, med) }
                .toObservable()
            }
            .subscribeOn(uiScheduler)
            .subscribe {
                val event : MedicationStatusEvent = it.first
                val med : Medication = it.second

                _view.showPopupStatus("UPDATE: $med.name changed to ${event.status.status.name} status!")
            })
    }

    fun detachView () {
        if (!_disposables.isDisposed) {
            _disposables.dispose()
        }
    }

    fun selectedBarcodeScanner () {
        _view.showBarcodeScanner()
    }

    fun scannedBarcode (data:String) {
        val qrMed = Gson().fromJson(data, QrMedicine::class.java)
        val createMed = CreateMedication(qrMed.name, qrMed.maxQuantity, qrMed.milligrams,
            Duration.ofHours(qrMed.duration.toLong()))

        interactor.createMedication(createMed)
            .flatMapSingle { retrieveAll() }
            .subscribeOn(uiScheduler)
            .subscribe{ _view.renderItems(it) }
    }

    private fun retrieveAll () : Single<List<MedicationItem>> {
        return medicationRepository.all()
            .flatMap { Observable.fromIterable(it) }
            .map { med ->
                MedicationItem(med.id.unique,
                    med.name, med.status.toString(), "00:00")}
            .toList()
    }
}