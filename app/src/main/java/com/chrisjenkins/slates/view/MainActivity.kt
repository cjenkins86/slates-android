package com.chrisjenkins.slates.view

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.chrisjenkins.slates.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CAMERA_PERMISSION = 10
    }

    private lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        router = Conductor.attachRouter(this, controller_container,
            savedInstanceState)

        if (isCameraPermissionGranted()) {
            router.setRoot(RouterTransaction.with(MedicationListController()))
        }
    }

    private fun isCameraPermissionGranted () : Boolean {
        val perm = ContextCompat.checkSelfPermission(baseContext, Manifest.permission.CAMERA)
        return perm == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (isCameraPermissionGranted()) {
                router.setRoot(RouterTransaction.with(MedicationListController()))

            } else {
                Toast.makeText(this, "Need camera permission!", Toast.LENGTH_SHORT)
                    .show()
                finish()
            }
        }
    }
}
