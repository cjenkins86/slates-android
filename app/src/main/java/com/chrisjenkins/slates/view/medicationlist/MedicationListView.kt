package com.chrisjenkins.slates

interface MedicationListView {
    fun showBarcodeScanner ()
    fun showPopupStatus (message:String)
    fun renderItems (items: List<MedicationItem>)
}