package com.chrisjenkins.slates

import android.app.Activity
import android.content.Intent
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import com.bluelinelabs.conductor.Controller
import kotlinx.android.synthetic.main.medication_list_controller.view.*
import javax.inject.Inject

class MedicationListController : Controller(), MedicationListView {

    @Inject lateinit var presenter: MedicationListPresenter

    override fun onCreateView(@NonNull inflater: LayoutInflater, @NonNull container: ViewGroup): View {
        val view = inflater.inflate(R.layout.medication_list_controller, container,false)

        (activity!!.application as SlatesApplication).medicationListComponent.inject(this)

        view.scan_barcode.setOnClickListener { presenter.selectedBarcodeScanner() }

        return view
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        presenter.attachView(this)
    }

    override fun onDetach(view: View) {
        super.onDetach(view)

        presenter.detachView()
    }

    override fun showPopupStatus(message: String) {
        // .. show popup dialog of medication status update
    }

    override fun renderItems (items: List<MedicationItem>) {
        // .. set items in recycler
    }

    override fun showBarcodeScanner() {
        // .. show barcode scanner activity
        startActivityForResult(Intent(activity, BarcodeActivity::class.java), Activity.RESULT_OK)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == AppCompatActivity.RESULT_OK) {
            val qr = data!!.getStringExtra("qr")!!
            presenter.scannedBarcode(qr)
        }
    }

}