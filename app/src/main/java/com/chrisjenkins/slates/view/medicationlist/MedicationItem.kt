package com.chrisjenkins.slates

data class MedicationItem (val id:String, val name:String, val status:String, val timeLeft:String)
