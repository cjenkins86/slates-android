package com.chrisjenkins.slates

data class QrMedicine (val name:String, val maxQuantity:Int, val milligrams:Int, val duration:Int)