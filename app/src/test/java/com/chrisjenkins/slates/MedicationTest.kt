package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.entities.Medication
import com.chrisjenkins.slates.domain.events.DomainEvent
import com.chrisjenkins.slates.domain.events.onDomainEvent
import com.chrisjenkins.slates.domain.valueobjects.Dosage
import com.chrisjenkins.slates.domain.valueobjects.MedicationStatus
import com.chrisjenkins.slates.integration.locators.MedicationDateLocator
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Test
import org.threeten.bp.Duration
import org.threeten.bp.Instant

class MedicationTest {
    @Test
    fun takeOneDosage() {
        var curDate = Instant.now()
        var mockedDate : MedicationDate = mock()
        whenever(mockedDate.currentTime()).thenReturn(curDate)
        MedicationDateLocator.date = mockedDate

        val medication = Medication(
            MedicationId("100"),
            "Tylenol",
            10,
            600,
            Duration.ofHours(6)
        )

        val medTestObs = TestObserver<DomainEvent>()
        onDomainEvent.subscribe(medTestObs)

        medication.takeDosage()

        Assert.assertEquals(medication.dosages, listOf(
            Dosage(
                medication.milligrams,
                curDate
            )
        ))

        medTestObs.assertValue {
            it.name == TookMedicationDosage.NAME &&
            (it as TookMedicationDosage).medId == medication.id
        }
    }

    @Test
    fun takeManyDosages() {
        var date1 = Instant.now()
        var date2 = Instant.now().plusMillis(10000)
        var date3 = Instant.now().plusMillis(20000)

        var mockedDate : MedicationDate = mock()

        val medication = Medication(
            MedicationId("100"),
            "Tylenol",
            10,
            600,
            Duration.ofHours(6)
        )

        val medTestObs = TestObserver<DomainEvent>()
        onDomainEvent.subscribe(medTestObs)

        whenever(mockedDate.currentTime()).thenReturn(date1)
        MedicationDateLocator.date = mockedDate
        medication.takeDosage()

        whenever(mockedDate.currentTime()).thenReturn(date2)
        MedicationDateLocator.date = mockedDate
        medication.takeDosage()

        whenever(mockedDate.currentTime()).thenReturn(date3)
        MedicationDateLocator.date = mockedDate
        medication.takeDosage()

        Assert.assertEquals(medication.dosages,
            listOf(
                Dosage(medication.milligrams, date1),
                Dosage(medication.milligrams, date2),
                Dosage(medication.milligrams, date3)
            ))

        medTestObs.assertValueCount(3)
    }

    @Test
    fun failedNoRemainingDosages() {
        val medication = Medication(
            MedicationId("100"),
            "Tylenol",
            1,
            600,
            Duration.ofHours(6)
        )

        val medTestObs = TestObserver<DomainEvent>()
        onDomainEvent.subscribe(medTestObs)

        var mockedDate : MedicationDate = mock()

        val date1 = Instant.now()
        whenever(mockedDate.currentTime()).thenReturn(date1)
        MedicationDateLocator.date = mockedDate
        medication.takeDosage()

        val date2 = Instant.now().plusMillis(10000)
        whenever(mockedDate.currentTime()).thenReturn(date2)
        MedicationDateLocator.date = mockedDate
        medication.takeDosage()

        Assert.assertEquals(medication.dosages, listOf(
            Dosage(
                medication.milligrams,
                date1
            )
        ))

        medTestObs.assertValueCount(1)
    }

    @Test
    fun startWithDefaultStatus() {
        val med = Medication(
            MedicationId("100"),
            "Tylenol", 10, 600, Duration.ofHours(6)
        )

        Assert.assertEquals(med.status, MedicationStatus.Unknown)
    }

    @Test
    fun changeToOverdueAfterWaiting () {
        val med = Medication(
            MedicationId("100"),
            "Tylenol", 10, 600, Duration.ofHours(6)
        )

        var medStatus : MedicationStatus = MedicationStatus.Unknown
        var mockedDate : MedicationDate = mock()

        med.takeDosage()

//        whenever(mockedDate.currentTime()).thenReturn(date)
//        MedicationDateLocator.date = mockedDate

//        med.status.subscribe { status -> medStatus = status }
//
//        Assert.assertEquals(medStatus, MedicationStatus.DoseIsOverdue)
    }

    @Test
    fun changeToWaitingAfterOverdue() {

    }

    @Test
    fun changeToCompletedAfterTakingLastDose () {

    }
}
