package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.entities.Medication
import com.chrisjenkins.slates.domain.events.DomainEventId
import com.chrisjenkins.slates.domain.events.triggerDomainEvent
import com.chrisjenkins.slates.integration.events.MedicationStatusEvent
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.mockito.Mockito
import org.threeten.bp.Duration

class MedicationDosageHandlerTest {
    @Test
    fun scheduleDosageTimerAfterDomainEvent () {
        val med = Medication(MedicationId("1"), "Tylenol", 10,
            600, Duration.ofHours(6))
        val medRepo = MedicationMemoryRepository(listOf(med))
        val medScheduler = Mockito.mock(MedicationScheduler::class.java)
        val medId = MedicationId("1")
        val scheduleStatus = MedicationScheduleStatus(
            med.id.unique, ScheduleStatusType.WaitingToComplete)

        val medTestObs = TestObserver<IntegrationEvent>()
        onIntegrationEvent.subscribe(medTestObs)

        MedicationDosageHandler(medScheduler, medRepo)

        whenever(medScheduler.start(any(), any())).thenReturn(Observable.just(scheduleStatus))

        triggerDomainEvent(TookMedicationDosage(DomainEventId("1"), med.id))

        verify(medScheduler, atLeastOnce()).start(medId, 6)

        medTestObs.assertValue {
                event -> event.name ==  MedicationStatusEvent.NAME &&
                (event as MedicationStatusEvent).status == scheduleStatus
        }
    }
}