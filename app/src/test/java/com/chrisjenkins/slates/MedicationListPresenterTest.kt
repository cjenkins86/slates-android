package com.chrisjenkins.slates

import com.chrisjenkins.slates.domain.entities.Medication
import com.chrisjenkins.slates.integration.events.MedicationStatusEvent
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.atMost
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import org.mockito.Mockito
import org.threeten.bp.Duration

class MedicationListPresenterTest {
    @Test
    fun retrieveMedicationsForViewOnAttach () {
        val med = Medication(MedicationId("1"), "Tylenol", 10,
            600, Duration.ofHours(6))
        val medRepo = MedicationMemoryRepository(listOf(med))

        val view = Mockito.mock(MedicationListView::class.java)

        val uiTestScheduler = TestScheduler()
        val presenter = MedicationListPresenter(medRepo, uiTestScheduler)

        presenter.attachView(view)

        uiTestScheduler.triggerActions()

        verify(view, atLeastOnce()).renderItems(listOf(
            MedicationItem(med.id.unique, med.name, med.status.toString(), "00:00")))
    }

    @Test
    fun showPopupWhenMedicationStatusChanged () {
        val med = Medication(MedicationId("1"), "Tylenol", 10,
            600, Duration.ofHours(6))
        val medRepo = MedicationMemoryRepository(listOf(med))

        val view = Mockito.mock(MedicationListView::class.java)

        val uiTestScheduler = TestScheduler()
        val presenter = MedicationListPresenter(medRepo, uiTestScheduler)

        presenter.attachView(view)

        uiTestScheduler.triggerActions()

        val event = MedicationStatusEvent(IntegrationEventId("1"),
            MedicationScheduleStatus(med.id.unique, ScheduleStatusType.Done))

        triggerIntegrationEvent(event)

        verify(view, atLeastOnce()).showPopupStatus("UPDATE: $med.name changed to ${event.status.status.name} status!")
    }

    @Test
    fun disconnectEventListerWhenDetachView () {
        val med = Medication(MedicationId("1"), "Tylenol", 10,
            600, Duration.ofHours(6))
        val medRepo = MedicationMemoryRepository(listOf(med))

        val view = Mockito.mock(MedicationListView::class.java)

        val uiTestScheduler = TestScheduler()
        val presenter = MedicationListPresenter(medRepo, uiTestScheduler)

        presenter.attachView(view)

        presenter.detachView()

        uiTestScheduler.triggerActions()

        val event = MedicationStatusEvent(IntegrationEventId("1"),
            MedicationScheduleStatus(med.id.unique, ScheduleStatusType.Done))

        triggerIntegrationEvent(event)

        verify(view, atMost(0))
            .showPopupStatus("UPDATE: $med.name changed to ${event.status.status.name} status!")

    }
}